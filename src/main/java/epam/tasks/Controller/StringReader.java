package epam.tasks.Controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class StringReader {
    int countUnique;
    List<String> list;
    Scanner scanner;
    String text;
    Map<String, Integer> map;

    public StringReader() {
        countUnique = 0;
        scanner = new Scanner(System.in);
        list = new ArrayList<>();
    }

    public List<String> getScan() {
        text = new String();
        String line = scanner.nextLine();
        while (!line.trim().isEmpty()) {
            list.add(line);
            if (text.isEmpty()) {
                text = line;
            } else {
                text += " " + line;
            }
            line = scanner.nextLine();
        }
        return list;
    }

    public Set<String> getUniqueWordsSet(List<String> list) {
        Set<String> set = new HashSet<>();
        map = new HashMap<>();
        for (String line : list) {
            String arr[] = line.split(" ");
            for (int i = 0; i < arr.length; i++) {
                if (set.add(arr[i])) {
                    countUnique++;
                }
            }
        }
        return set;
    }

    public int getUniqueCount() {
        return countUnique;
    }

    public void showOccuranceOfWords(String line) {
        map = new HashMap<>();
        String[] words = line.split(" ");
        map = putInMap(words);
        map.keySet().stream().forEach(i -> System.out.println(i + " : " + map.get(i)));
    }

    public String getFullText() {
        return text;
    }

    public void showOccuranceOfLetters(String text) {
        map = new HashMap<>();
        //No upper-case letters or spaces
        String formattedText = text.replaceAll("[\\sA-Z]+", "");
        //splits to letters
        String[] letters = formattedText.split("");
        map = putInMap(letters);
        map.keySet().stream().forEach(i -> System.out.println(i + " : " + map.get(i)));
    }

    private Map<String, Integer> putInMap(String[] text) {
        Map<String, Integer> map = new HashMap<>();
        for (int i = 0; i < text.length; i++) {
            if (!(map.get(text[i]) == null)) {
                int occurance = map.get(text[i]);
                occurance++;
                map.put(text[i], occurance);
            } else {
                map.put(text[i], 1);
            }
        }
        return map;
    }
}

