package epam.tasks.Controller;

import epam.tasks.Model.Commands.Command;

public class CommandController {
    Command command;
   public CommandController(){
    }
    CommandController(Command command){
        this.command = command;
    }
    public void execute(String arg){
        command.make(arg);
    }
    public void setCommand(Command command){
        this.command = command;
    }
}
