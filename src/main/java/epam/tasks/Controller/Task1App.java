package epam.tasks.Controller;

import epam.tasks.Model.MyOperator;

public class Task1App {
        public static void appTask1() {
            MyOperator myOperator = (a, b, c) -> {
                int max = a;
                max = max < b? b: max;
                max = max < c? c: max;
                return max;
            };
            System.out.println("Max is: "+myOperator.operate(4,12,1));
            myOperator = (a,b,c) -> (a+b+c)/3;
            System.out.println("Average is: "+myOperator.operate(4,12,1));
        }
}
