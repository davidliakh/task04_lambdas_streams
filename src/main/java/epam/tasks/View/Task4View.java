package epam.tasks.View;

import epam.tasks.Controller.StringReader;

import java.util.List;
import java.util.Set;

public class Task4View {
    public static void main(String[] args) {
        StringReader stringReader = new StringReader();
        System.out.println("Enter lines:");
        List<String> lines = stringReader.getScan();
        lines.forEach(System.out::println);
        Set<String> uniqueWordsSet = stringReader.getUniqueWordsSet(lines);
        int count = stringReader.getUniqueCount();
        System.out.println("Count of unique: " + count);
        System.out.println("Sorted unique words");
        uniqueWordsSet.stream().sorted(String::compareTo).forEach(System.out::println);
        System.out.println("Occurance of words:");
        stringReader.showOccuranceOfWords(stringReader.getFullText());
        System.out.println("Occurance of letters:");
        stringReader.showOccuranceOfLetters(stringReader.getFullText());
    }
}
