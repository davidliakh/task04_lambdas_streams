package epam.tasks.View;

import epam.tasks.Controller.RandomList;

import java.util.List;

public class Task3View {
    public static void main(String[] args) {
        List<Integer> list = RandomList.getRandomList(10, 0, 100);
        list.forEach(System.out::println);
        Integer max = list.stream().max(Integer::compareTo).get();
        Integer min = list.stream().min(Integer::compareTo).get();
        Integer average = list.stream().reduce((left, right) -> left + right).get() / list.size();
        Integer sum = list.stream().reduce((left, right) -> left + right).get();
        Integer anotherSum = list.stream().mapToInt(i -> i.intValue()).sum();
        long biggerThanAverageCount = list.stream().filter(i -> i > average).count();
        System.out.println("Max using stream().max(): " + max);
        System.out.println("Max using stream().min(): " + min);
        System.out.println("Average: " + average);
        System.out.println("Sum using reduce: " + sum);
        System.out.println("Sum using sum: " + anotherSum);
        System.out.println("Bigger than average: " + biggerThanAverageCount);
    }
}
