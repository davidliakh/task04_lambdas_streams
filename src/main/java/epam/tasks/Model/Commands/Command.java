package epam.tasks.Model.Commands;

@FunctionalInterface
public interface Command {
    void make(String arg);
}

