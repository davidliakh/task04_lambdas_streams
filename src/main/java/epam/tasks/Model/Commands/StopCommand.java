package epam.tasks.Model.Commands;

public class StopCommand implements Command {

    @Override
    public void make(String arg) {
        Command command = StopCommand::stop;
        command.make(arg);
    }

    private static void stop(String arg) {
        System.out.println("Stopped: " + arg);
    }
}
