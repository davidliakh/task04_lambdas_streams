package epam.tasks.Model.Commands;

public class GoCommand implements Command {

    @Override
    public void make(String arg) {
        Command command = (a) -> {
            System.out.println("Going: " + arg);
        };
        command.make(arg);
    }
}
