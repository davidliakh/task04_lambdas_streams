package epam.tasks.Model;

    @FunctionalInterface
    public interface MyOperator {
        int operate(int a,int b, int c);
    }
